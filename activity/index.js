console.log("S4 Activity")

// S24 Activity Template:

/*Item 1.)
- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/
// Code here:

function cube(num) {
	let ccube = num ** 3
	console.log(`The cube of ${num} is ${ccube}`)
}

let getCube = cube(2)
getCube = cube(7)

/*Item 2.)
- Create a variable address with a value of an array containing details of an address.
- Destructure the array and print out a message with the full address using Template Literals.*/
// Code here:


let address = ["258 Washington Ave", "NW","California", 9011]
/*let address = {
	street: "258 Washington Ave",
	city: "NW",
	state: "California",
	zipcode: 9011
}*/

let [street, city, state, zipcode] = address
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`)


/*Item 3.)
- Create a variable animal with a value of an object data type with different animal details as its properties.
- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
// Code here:
let animal ={
	type: "crocodile",
	name: "Lolong",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {type, name, weight, measurement} = animal

function showAnimal(name, type, weight, measurement) {
	console.log(`${name} was a saltwater ${type}. He weighed at ${weight}kgs with a measurement of ${measurement}`)
}

showAnimal(name, type, weight, measurement)



/*Item 4.)
- Create an array of numbers.
- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

// Code here:

const arrNum = [1, 2, 3, 4, 5, 15];
let number

arrNum.forEach((number) => console.log(number));

/*Item 5.)
- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
- Create/instantiate a new object from the class Dog and console log the object.*/

// Code here:*/

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const myDog = new Dog("Frankie", 5, "Miniature Daschund");
console.log(myDog);
