console.log("Javascript ES6 Updates")
console.log("")

// ES6 Updates
// Exponent Operator: we use "**" for exponent

const firstNum = Math.pow(8,2)
console.log(firstNum)

const secondNum = 8 ** 2
console.log(secondNum)

// 5 raised to the power of 5
const thirdNum = 5 ** 5
console.log(thirdNum)

/*
	Template Literals
	- allows us to write strings without using the concatination operator (+)
*/

let name = "George"

/*
	Concatination/Pre-Template Literal
	- uses single Quote (' ')
*/

let message = 'Hello' + name + 'Welcome to Zuitt Coding Bootcamp!'
console.log("Message without template literals: " + message)

console.log("")

/*
	Strings Using Template Literal
	- uses the backticks (``)
*/

message = `'Hello ${name}. Welcome to Zuitt Coding Bootcamp!`
console.log(`Message with template literal: ${message}`)

let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the solution of ${secondNum}
`

console.log(anotherMessage)

anotherMessage = "\n" + name  + ' attended a math competion. \n He won it by solving the problem 8**2 with the solution ' + secondNum + '. \n'

console.log(anotherMessage)

// Operation inside template  template literal
const interestRate = .1
const principal = 1000

console.log(`The interest on your savings is: ${principal * interestRate}`)

/*
	Array destructuring
	- allows to unpack elements in an array into distinct variables; allows us to name the array elements with variables instead of index number.

	Syntax:
	let/cons [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"]
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

/*
	Array Desctructing
*/

const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

/*
	Object Destructing
	- allows to unpack properties of objects into distinct variable. Shortends the syntac for accessing properties from objects

	Syntax:
	let/const {propertyName, propertyName, propertyName} = objectName
*/

console.log("")
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-Object Destructioring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

function getFullName(givenName, maindenName, familyName) {
	console.log(`${givenName} ${maindenName} ${familyName}`)
}

getFullName(person.givenName, person.maidenName, person.familyName)

// Using Object Destructuring
const {maidenName, familyName, givenName} = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)

function getFullName(given, maiden, family) {
	console.log(`${given} ${maiden} ${family}`)
}

getFullName(givenName, maidenName, familyName)

console.log("")

const employees = ['Silver', 'Botbot', 'Ayo']

const [employee1, employee2, employee3] = employees;

console.log(`Employees are: 
	1. ${employee1}
	2. ${employee2}
	3. ${employee3}`)


const pet = {
	breed: 'american bully',
	color: 'black',
	age: 3
}

const{breed, color, age} = pet 

console.log(`My pet is:
	breed: ${breed}
	color: ${color},
	age: ${age}`)

/*
	Pre-arrow Function and Arrow Function

	Syntax:
	function functionName (paramA, paramB) {
		statement/console.log/return
	}
*/

function printFullName (firstName, middleInitial, lastName) {
	console.log(firstName + " " + middleInitial + " " + lastName)
}

printFullName("Rupert", "B", "Ramos")

/*
	Arrow Function

	Syntax:
	let/const variableName = (paramA, paramB) => {
		statement/console.log/return
	}
*/

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName} `)
}

printFullName1("John Robert", "A", "Dela Vega")

const students = ["Yoby", "Emman", "Ronel"]

//Functions with Loop
//Pre-Arrow Functions

students.forEach(function(student) {
	console.log(student + " is a student")
})

//Arrow Functions

students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statement

//Pre-Arrow
function add (x,y){
	return x + y
}

let total = add(12,15)
console.log(total)

//Arrow
const addition = (x,y) => x+y 

/*const addition = (x,y) => {
	return x+y 
}*/

let total2 = addition(12,15)
console.log(total2)

// Default Function Argument Value

const greet = (name = "User") => {
	return `Good evening, ${name}`
}

console.log(greet())
console.log(greet("Archie"))

/*
	Class-Based Object Blueprint
	-allows creation/instantiation of objects using class as blueprint

	Syntax:
	class className {
		constructor (objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA
			this.objectPropertyB = objectPropertyB
		}
	}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = 2021

console.log(myCar)

const myNewCar = new Car("Toyota", "Vios", 2019)
console.log(myNewCar)











